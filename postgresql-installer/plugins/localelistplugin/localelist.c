#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include "pluginapi.h"
#include "localelist.h"

HINSTANCE g_hInstance;

HWND g_hwndParent;


BOOL WINAPI DllMain(HANDLE hInst, ULONG ul_reason_for_call, LPVOID lpReserved)
{
    g_hInstance = hInst;
	return TRUE;
}

static UINT_PTR PluginCallback(enum NSPIM msg)
{
  return 0;
}

char StringSystemLocales[100000];

BOOL CALLBACK EnumLocalesProc (LPTSTR lpLocaleString)
{
	TCHAR country[128] = {0};
	TCHAR language[128] = {0};
    LCID localeID = 0;
    
	if(!sscanf(lpLocaleString,"%x", &localeID ))
		return FALSE;

	if(!GetLocaleInfo(localeID, LOCALE_SENGCOUNTRY, country, 128))
		return FALSE;
	if(!GetLocaleInfo(localeID, LOCALE_SENGLANGUAGE, language, 128))
		return FALSE;

	if(StringSystemLocales[0])
		strcat(StringSystemLocales, "|"); 
	strcat(StringSystemLocales, language); 
	strcat(StringSystemLocales, "_"); 
	strcat(StringSystemLocales, country); 

    return TRUE;
}




NSISFunction(GetStringSystemLocales)
{
	PLUGIN_INIT();
	extra->RegisterPluginCallback(g_hInstance, PluginCallback);
	{
		StringSystemLocales[0] = 0;
		strcat(StringSystemLocales, "C");
		EnumSystemLocales (EnumLocalesProc, LCID_INSTALLED);
	
		//pushstring("GetStringSystemLocales Ok");
		pushstring(StringSystemLocales);
		return;
	}
}